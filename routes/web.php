<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/api/bebewash/', 'namespace' => 'Api', 'middleware' => []], function() {
   	Route::post('/login', 'AuthController@login');
	Route::post('/logout', 'AuthController@logout');
	Route::post('/forgot-password', 'AuthController@forgotPassword');
	Route::post('/change-password', 'AuthController@changePassword'); // use token
	Route::get('/authenticated', 'AuthController@authenticated'); // use token

	Route::resource('/banks', 'BankAccountController'); // use token
	Route::post('/banks/multiple-delete', 'BankAccountController@multipleDestroy'); // use token

	Route::resource('/vehicles', 'VehicleController'); // use token
	Route::post('/vehicles/multiple-delete', 'VehicleController@multipleDestroy'); // use token

	Route::resource('/prices', 'PriceController'); // use token
	Route::post('/prices/multiple-delete', 'PriceController@multipleDestroy'); // use token

	Route::resource('/items', 'ItemController'); // use token
	Route::post('/items/multiple-delete', 'ItemController@multipleDestroy'); // use token

	Route::resource('/agents', 'AgentController'); // use token
	Route::post('/agents/multiple-delete', 'AgentController@multipleDestroy'); // use token

	Route::resource('/customers', 'CustomerController'); // use token
	Route::post('/customers/multiple-delete', 'CustomerController@multipleDestroy'); // use token
});

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => '/dashboard'], function() {
	Route::get('/', 'DashboardController@index')->name('dashboard.index');

	Route::get('list-bank', 'BankAccountController@index')->name('master.banks.index');
	Route::get('create-bank', 'BankAccountController@create')->name('master.banks.create');
	Route::get('bank/{id}/edit-bank', 'BankAccountController@edit')->name('master.banks.edit');

	Route::get('list-vehicle', 'VehicleController@index')->name('master.vehicles.index');
	Route::get('create-vehicle', 'VehicleController@create')->name('master.vehicles.create');
	Route::get('vehicle/{id}/edit-vehicle', 'VehicleController@edit')->name('master.vehicles.edit');

	Route::get('list-agent', 'AgentController@index')->name('master.agents.index');
	Route::get('create-agent', 'AgentController@create')->name('master.agents.create');
	Route::get('agent/{id}/edit-agent', 'AgentController@edit')->name('master.agents.edit');

	Route::get('list-item', 'ItemController@index')->name('master.items.index');
	Route::get('create-item', 'ItemController@create')->name('master.items.create');
	Route::get('item/{id}/edit-item', 'ItemController@edit')->name('master.items.edit');

	Route::get('list-customer', 'CustomerController@index')->name('master.customers.index');
	Route::get('create-customer', 'CustomerController@create')->name('master.customers.create');
	Route::get('customer/{id}/edit-customer', 'CustomerController@edit')->name('master.customers.edit');
});
