<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $obj = new \App\User();
        $obj->name = $faker->name;
        $obj->email = 'setiawanerick1506@gmail.com';
        $obj->role = 'admin';
        $obj->password = bcrypt('test123');
        $obj->save();

        for($i = 0;$i<3;$i++) {
            $faker = Faker\Factory::create();
            $obj = new \App\User();
            $obj->name = $faker->name;
            $obj->email = 'admin'.($i+1).'@bebewash.com';
            $obj->role = 'admin';
            $obj->password = bcrypt('admin');
            $obj->save();
        }
    }
}
