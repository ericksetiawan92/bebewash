<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_id');
            $table->string('type')->nullable();
            $table->text('description')->nullable();
            $table->string('group')->nullable();
            $table->string('subcategory')->nullable();
            $table->integer('price_id')->nullable()->unsigned();
            $table->string('name_of_product')->nullable();
            $table->string('name_of_service')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('items', function($table) {
            $table->foreign('price_id')->references('id')->on('prices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
