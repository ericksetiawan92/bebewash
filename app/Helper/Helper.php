<?php 

namespace App\Helper;

use App\User;
use App\Article;
use Auth;
use Carbon\Carbon;

class Helper {
	public static function englishDateFormat($timestamp='', $dateFormat='l, j F Y')
    {
		if (trim ($timestamp) == '')
		{
			$timestamp = time ();
		}
		elseif (!ctype_digit ($timestamp))
		{
			$timestamp = strtotime ($timestamp);
		}
		
		# remove S (st,nd,rd,th) there are no such things in indonesia
		$dateFormat = preg_replace ("/S/", "", $dateFormat);
		$pattern = array
		(
			'/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
			'/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
			'/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
			'/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
			'/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
			'/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
			'/April/','/June/','/July/','/August/','/September/','/October/',
			'/November/','/December/',
		);
		$replace = array
		(
			'Mon','Tue','Wed','Thrus','Fri','Sat','Sun',
			'Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday',
			'Jan','Feb','Mar','Apr','May','Jun','Jul','Ags','Sep','Oct','Nov','Dec',
			'January','February','March','April','Juny','July','August','September',
			'October','November','December',
		);
		$date = date ($dateFormat, $timestamp);
		$date = preg_replace ($pattern, $replace, $date);
		$date = "{$date}";
		return $date;
	}

	public static function getProfilePicture() 
	{
		// if (Auth::check()) {
		// 	$image = Auth::user()->images()->where('type', Image::TYPE_PROFILE_PICTURE)->first();

		// 	return ($image) ? 'public/'.$image->file_path : null;
		// }

		// return null;
	}

	public static function getBreadcrumb() {
		$currentPath = \Request::path();
		$explode = explode("/", $currentPath);

		$result = [];
		$url   = env('APP_URL');
		$count = count($explode);
		for ($i = 0; $i < 2; $i++) {
			$url .= $explode[$i] . '/'; 

			$index = ucwords(preg_replace('/[^\p{L}\p{N}\s]/u', ' ', $explode[$i]));
			switch ($index) {
	    		case 'Dashboard':
	    			$icon = 'fa fa-dashboard';
					break;
				case 'Articles':
	    			$icon = 'fa fa-book';
					break;
				case 'Categories':
	    			$icon = 'fa fa-bars';
					break;
				case 'Partners':
				case 'Users':
	    			$icon = 'fa fa-users';
					break;
				case 'Responses':
					$icon = 'fa fa-address-book';
					break;
				case 'Pages':
	    			$icon = 'fa fa-file';
					break;
	    	}
			
			$result[$index]['url']  = $url;
			$result[$index]['icon'] = $icon;
		}

		return $result;
	}

	public static function renderMonth($month)
	{
		$months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

		return [
			'number' => (int) $month,
			'name' => $months[$month-1]
		];
	}

	public static function renderYear()
	{
		$defaultYear = Carbon::parse('2018-01-02');
		$currentYear = Carbon::now();

		$diff = 0;
		if ($currentYear->diffInYears($defaultYear) === 0) {
			$diff = 1;
		} else {
			$diff = $currentYear->diffInYears($defaultYear) + 1;
		}

		$years = [];
		$default = 2018;
		for ($i=0; $i<=$diff; $i++) {
			$years[] = [
				'number' => $default,
				'name' => $default
			];

			$default++;
		}

		return $years;
	}

	public static function getUnreadNotification()
	{
		// $notifications = Notification::whereNull('user_id')->whereNull('read_at')->get();

		// return (count($notifications)) ? count($notifications) : null;
	}

	public static function getListNotifications()
	{
		// $notifications = Notification::whereNull('user_id')->get();

		// $array = [];
		// foreach ($notifications as $notification) {
		// 	$data = ($notification->data) ? json_decode($notification->data) : null;
        // 	$scope = $notification->scope;
        // 	$type = $notification->type;

	    //     $description = null;
	    //     $id = null;
	    //     switch ($scope) {
	    //         case 'LAND':
	    //         	switch ($type) {
	    //         		case 'CREATE':
	    //         			$user = User::find($data->creator_id);
	    //         			$description = $user->name . ' mendaftarkan lahan baru untuk dijadikan project';
	    //         			break;
	    //             case 'RESEND':
	    //             	$user = User::find($data->creator_id);
	    //             	$description = $user->name . ' mendaftarkan ulang lahannya untuk dijadikan project';
	    //               break;
	    //         		case 'ACCEPT':
	    //         			$id = $data->land_id;
	    //         			$user = User::find($data->creator_id);
	    //         			$land = Land::find($data->land_id);
	    //         			$description = $user->name . ' menerima lahan anda dengan ID: ' . $land->id;
	    //         			break;
	    //         		case 'REJECT':
	    //         			$id = $data->land_id;
	    //         			$user = User::find($data->creator_id);
	    //         			$land = Land::find($data->land_id);
	    //         			$description = $user->name . ' menolak lahan anda dengan ID: ' . $land->id . ' karena ' . strtolower($land->rejection_note);
	    //         			break;
	    //         	}
	    //          	break;

	    //         case 'COMMENTAR':
	    //         	switch ($type) {
	    //                 case 'SEND':
	    //                 		$id = $data->land_id;
	    //                     $user = User::find($data->creator_id);
	    //                     $land = Land::find($data->land_id);
	    //                     $description = $user->name . ' mention anda di komentarnya.';
	    //                     break;
	    //                 case 'SEND-TO-LAND-OWNER':
	    //                 		$id = $data->land_id;
	    //                     $user = User::find($data->creator_id);
	    //                     $land = Land::find($data->land_id);
	    //                     $description = $user->name . ' memberikan komentar di lahan anda.';
	    //                     break;
	    //             }
	    //             break;

	    //         case 'PROJECT':
	    //             switch ($type) {
	    //             	case 'CREATE':
	    //             				$id = $data->project_id;
	    //                     $project = Project::find($data->project_id);
	    //                     $description = 'Project baru telah di buat ' . $project->name . '. Masa pendanaan dimulai';
	    //                     break;
	    //                 case 'JOIN':
	    //                 		$id = $data->project_id;
	    //                     $user = User::find($data->creator_id);
	    //                     $project = Project::find($data->project_id);
	    //                     $description = $user->name . ' mengajukan request untuk bergabung pada project ' . $project->name . '.';
	    //                     break;
	    //                 case 'START':
	    //                 		$id = $data->project_id;
	    //                     $project = Project::find($data->project_id);
	    //                     $description = 'Project ' . $project->name . ' sudah dimulai.';
	    //                     break;
	    //                 case 'END':
	    //                 		$id = $data->project_id;
	    //                     $project = Project::find($data->project_id);
	    //                     $description = 'Project ' . $project->name . ' sudah berakhir.';
	    //                     break;
	    //                 case 'ACCEPT':
	    //                 		$id = $data->project_id;
	    //                     $project = Project::find($data->project_id);
	    //                     $description = 'Permintaan gabung project anda untuk project ' . $project->name . ' telah diterima.';
	    //                     break;
	    //                 case 'REJECT':
	    //                 		$id = $data->project_id;
	    //                     $project = Project::find($data->project_id);
	    //                     $description = 'Permintaan gabung project anda untuk project ' . $project->name . ' telah ditolak.';
	    //                     break;
	    //             }
	    //             break;

	    //         case 'DEPOSIT':
	    //             switch ($type) {
	    //                 case 'ACCEPT':
	    //                     $id = $data->deposit_id;
	    //                     $deposit = Deposit::find($data->deposit_id);
	    //                     $description = $deposit->masterDeposit->name . ' ' . $deposit->amount . ' anda diterima.';
	    //                     break;
	    //                 case 'REJECT':
	    //                     $id = $data->deposit_id;
	    //                     $deposit = Deposit::find($data->deposit_id);
	    //                     $description = $deposit->masterDeposit->name . ' ' . $deposit->amount . ' anda ditolak.';
	    //                     break;
	    //             }
	    //             break;
	    //         case 'FINANCE-WITHDRAW':
        //         switch ($type) {
        //             case 'ACCEPT':
        //                 $id = $data->finance_id;
        //                 $finance = Finance::find($data->finance_id);
                        
        //                 $name = null;
        //                 if ($finance->masterFinanceFrom->is_pokok) $name = 'Simpanan Pokok';
        //                 else if ($finance->masterFinanceFrom->is_wajib) $name = 'Simpanan Wajib';
        //                 else if ($finance->masterFinanceFrom->is_sukarela) $name = 'Simpanan Sukarela';

        //                 $description = $name . ' ' . $finance->amount . ' anda diterima.';
        //                 break;
        //             case 'REJECT':
        //                 $id = $data->finance_id;
        //                 $finance = Finance::find($data->finance_id);

        //                 $name = null;
        //                 if ($finance->masterFinanceFrom->is_pokok) $name = 'Penarikan Simpanan Pokok';
        //                 else if ($finance->masterFinanceFrom->is_wajib) $name = 'Penarikan Simpanan Wajib';
        //                 else if ($finance->masterFinanceFrom->is_sukarela) $name = 'Penarikan Simpanan Sukarela';

        //                 $description = $name . ' ' . $finance->amount . ' anda ditolak.';
        //                 break;
        //         }
        //         break;
	    //     }

		// 	$array[] = [
		// 				'module_id' => $id,
	    //     	'user_id' => $notification->user_id,
	    //     	'scope' => $notification->scope,
	    //     	'type' => $notification->type,
	    //     	'description' => $description,
	    //     	'read_at' => Carbon::parse($notification->read_at)->setTimezone('Asia/Jakarta')->format('Y-m-d H:i'),
	    //     	'created_at' => Carbon::parse($notification->created_at)->setTimezone('Asia/Jakarta')->format('Y-m-d H:i')
	    //     ];
		// }

		// return $array;
	}
}