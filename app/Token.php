<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $table = 'tokens';
    protected $guarded = [];
    
    public $timestamps = true;

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
