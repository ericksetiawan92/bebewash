<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{   
    protected $guarded = [];

    public function bankAccounts()
    {
        return $this->hasMany('App\BankAccount');
    }
}
