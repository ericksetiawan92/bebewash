<?php

namespace App\Http\Controllers;

use App\Bank;
use App\BankAccount;
use App\Item;
use App\Price;
use App\Utils\CodeFactory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemController extends Controller
{
    function index() {
        return view('dashboard.items.index', [
            'menu' => 'item'
        ]);
    }

    function create() {
        return view('dashboard.items.create', [
            'menu' => 'item',
            'prices' => Price::all()
        ]);
    }

    function edit($id) {
        return view('dashboard.items.edit', [
            'menu' => 'item',
            'id' => $id,
            'prices' => Price::all()
        ]);
    }
}
