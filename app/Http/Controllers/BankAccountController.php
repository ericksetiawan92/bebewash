<?php

namespace App\Http\Controllers;

use App\Bank;
use App\BankAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankAccountController extends Controller
{
    function index() {
        return view('dashboard.banks.index', [
            'menu' => 'bank'
        ]);
    }

    function create() {
        return view('dashboard.banks.create', [
            'menu' => 'bank',
            'banks' => Bank::orderBy('name')->get()
        ]);
    }

    function edit($id) {
        return view('dashboard.banks.edit', [
            'menu' => 'bank',
            'id' => $id,
            'banks' => Bank::orderBy('name')->get()
        ]);
    }
}
