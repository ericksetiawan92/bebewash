<?php

namespace App\Http\Controllers;

use App\Vehicle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VehicleController extends Controller
{
    function index() {
        return view('dashboard.vehicles.index', [
            'menu' => 'vehicle'
        ]);
    }

    function create() {
        return view('dashboard.vehicles.create', [
            'menu' => 'vehicle'
        ]);
    }

    function edit($id) {
        return view('dashboard.vehicles.edit', [
            'menu' => 'vehicle',
            'id' => $id
        ]);
    }
}
