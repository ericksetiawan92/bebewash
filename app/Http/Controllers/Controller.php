<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use GuzzleHttp\Client;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendEmail($template, $receivers = [], $variables = [])
    {
        foreach ($receivers as $receiver) {
            $message = new Client();
            $info = null;
            $subject = null;

            switch ($template) {
                case 'FORGOT-PASSWORD':
                    $info = 'You had request to change your password. This is your temporary password : ' . $variables['new_password'] . '. Please login with this password and change the password as soon as possible.';
                    $subject = 'Reset your password';
                    break;
                case 'CHANGE-PASSWORD':
                    $info = 'Your password already changed.';
                    $subject = 'Change your password';
                    break;
            }

            try {
                \Mail::send('email_template', ['info' => $info], function($message) use ($subject, $receiver)
                {
                    $message->from('admin@dasavayu.com', 'Admin Dasavayu');
                    $message->subject($subject);
                    $message->to($receiver);
                });
            } catch (\Exception $e) {
                return true;
            }
        }
        
        return true;
    }
}
