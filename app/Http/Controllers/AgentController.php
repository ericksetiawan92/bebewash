<?php

namespace App\Http\Controllers;

use App\Agent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgentController extends Controller
{
    function index() {
        return view('dashboard.agents.index', [
            'menu' => 'agent'
        ]);
    }

    function create() {
        return view('dashboard.agents.create', [
            'menu' => 'agent'
        ]);
    }

    function edit($id) {
        return view('dashboard.agents.edit', [
            'menu' => 'agent',
            'id' => $id
        ]);
    }
}
