<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Utils\CodeFactory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    function index() {
        return view('dashboard.customers.index', [
            'menu' => 'customer'
        ]);
    }

    function create() {
        return view('dashboard.customers.create', [
            'menu' => 'customer',
        ]);
    }

    function edit($id) {
        return view('dashboard.customers.edit', [
            'menu' => 'customer',
            'id' => $id,
        ]);
    }
}
