<?php

namespace App\Http\Controllers\Api;

use App\Utils\CodeFactory;
use Validator;
use App\Bank;
use App\BankAccount;
use App\Token;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;

class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
        	'token'    => 'required'
        ]);

        if ($validator->fails()) {
        	return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
        			->first();

        if ($token) {
            $all = BankAccount::select([ 'id', 'bank_account_id','bank_id', 'account_number', 'account_name']);

            return DataTables::of($all)
                ->addColumn('check', function ($obj) {
                    return '
                    <input type="checkbox" name=id[] value='.$obj->id.'>
                ';
                })
                ->editColumn('bank_id', function (BankAccount $obj) {
                    return optional($obj->bank)->name;
                })
                ->addColumn('account', function (BankAccount $obj) {
                    return $obj->account_name . ' - '. $obj->account_number;
                })
                ->addColumn('actions', function ($obj) {
                    return '
                    <a class="mr-3" href="'.route("master.banks.edit", $obj->id).'"><img src="'.url("public/images/icons/edit.svg").'" alt="edit" width="16"></a>
                    <a href="javascript:void(0)" data-id="'.$obj->id.'" class="action-del"><img src="'.url("public/images/icons/trash.svg").'" alt="trash" width="16"></a>
                ';
                })
                ->rawColumns(['check','actions'])
                ->make(true);
        }

        return response()->json([
        	'success' => false,
        	'message' => 'Token not available'
        ], 500);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {

            return response()->json([
                'success' => true,
                'new_bank_account_id' => CodeFactory::simpleCode(BankAccount::class, 'B')
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'new_bank_account_id' => 'required',
            'bank_id' => 'required|numeric',
            'account_name' => 'required',
            'account_number' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $bank = Bank::find($request->input('bank_id'));
            if ($bank) {
                try {
                    $bankAccount = BankAccount::create([
                        'bank_account_id' => CodeFactory::simpleCode(BankAccount::class, 'B'),
                        'bank_id' => $request->input('bank_id'),
                        'account_name' => $request->input('account_name'),
                        'account_number' => $request->input('account_number')
                    ]);

                    return response()->json([
                        'success' => true,
                        'message' => 'Create bank account successful'
                    ]);
                } catch (\Exception $e) {
                    return response()->json([
                        'success' => false,
                        'message' => $e->getMessage()
                    ], 500);
                }
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Bank not found'
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $bankAccount = BankAccount::find($id);
            if (!$bankAccount) {
                return response()->json([
                    'success' => false,
                    'message' => 'Bank Account not found'
                ], 500);
            }

            $bankAccount = $this->data($bankAccount);

            return response()->json([
                'success' => true,
                'bank_account' => $bankAccount
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'bank_id' => 'required|numeric',
            'account_name' => 'required',
            'account_number' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $bank = Bank::find($request->input('bank_id'));
            if ($bank) {
                try {
                    $bankAccount = BankAccount::find($id);
                    if (!$bankAccount) {
                        return response()->json([
                            'success' => false,
                            'message' => 'Bank Account not found'
                        ], 500);
                    }
                    
                    $bankAccount->update([
                        'bank_id' => $request->input('bank_id'),
                        'account_name' => $request->input('account_name'),
                        'account_number' => $request->input('account_number')
                    ]);

                    return response()->json([
                        'success' => true,
                        'message' => 'Update bank account successful'
                    ]);
                } catch (\Exception $e) {
                    return response()->json([
                        'success' => false,
                        'message' => $e->getMessage()
                    ], 500);
                }
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Bank not found'
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            try {
                $bankAccount = BankAccount::find($id);
                if (!$bankAccount) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Bank Account not found'
                    ], 500);
                }
                    
                $bankAccount->delete();

                return response()->json([
                    'success' => true,
                    'message' => 'Delete bank account successful'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function multipleDestroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $ids = explode(",", $request->input('ids'));
            foreach ($ids as $id) {
                $bankAccount = BankAccount::find($id);
                if ($bankAccount) $bankAccount->delete();
            }

            return response()->json([
                'success' => true,
                'message' => 'Delete bank accounts successful'
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    // ============================================ Private Fuction ============================================ //

    private function dataArray($couriers)
    {
        $data = [];
        foreach ($couriers as $courier) {
            $data[] = [
                'id' => $courier->id,
                'bank_account_id' => $courier->bank_account_id,
                'bank' => [
                    'id' => $courier->bank->id,
                    'name' => $courier->bank->name
                ],
                'account_number' => $courier->account_number,
                'account_name' => $courier->account_name
            ];
        }

        return $data;
    }

    private function data($courier)
    {
        return [
            'id' => $courier->id,
            'bank_account_id' => $courier->bank_account_id,
            'bank' => [
                'id' => $courier->bank->id,
                'name' => $courier->bank->name
            ],
            'account_number' => $courier->account_number,
            'account_name' => $courier->account_name
        ];
    }
}
