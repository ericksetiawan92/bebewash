<?php

namespace App\Http\Controllers\Api;

use App\Utils\CodeFactory;
use Validator;
use App\Vehicle;
use App\Token;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
        	'token'    => 'required'
        ]);

        if ($validator->fails()) {
        	return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
        			->first();

        if ($token) {
            $all = Vehicle::select([ 'id', 'vehicle_id', 'number_plate']);

            return DataTables::of($all)
                ->addColumn('check', function ($obj) {
                    return '
                    <input type="checkbox" name=id[] value='.$obj->id.'>
                ';
                })
                ->editColumn('vehicle_id', function (Vehicle $obj) {
                    return $obj->vehicle_id;
                })
                ->addColumn('number_plate', function (Vehicle $obj) {
                    return $obj->number_plate;
                })
                ->addColumn('actions', function ($obj) {
                    return '
                    <a class="mr-3" href="'.route("master.vehicles.edit", $obj->id).'"><img src="'.url("public/images/icons/edit.svg").'" alt="edit" width="16"></a>
                    <a href="javascript:void(0)" data-id="'.$obj->id.'" class="action-del"><img src="'.url("public/images/icons/trash.svg").'" alt="trash" width="16"></a>
                ';
                })
                ->rawColumns(['check','actions'])
                ->make(true);

        	return response()->json([
	        	'success' => true,
                'vehicles' => $vehicles,
                'count' => $count
	        ]);
        }

        return response()->json([
        	'success' => false,
        	'message' => 'Token not available'
        ], 500);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            return response()->json([
                'success' => true,
                'new_vehicle_id' => CodeFactory::simpleCode(Vehicle::class, 'VHC')
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'new_vehicle_id' => 'required',
            'number_plate' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            try {
                $vehicle = Vehicle::create([
                    'vehicle_id' => CodeFactory::simpleCode(Vehicle::class, 'VHC'),
                    'number_plate' => $request->input('number_plate')
                ]);

                return response()->json([
                    'success' => true,
                    'message' => 'Create vehicle successful'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $vehicle = Vehicle::find($id);
            if (!$vehicle) {
                return response()->json([
                    'success' => false,
                    'message' => 'Vehicle not found'
                ], 500);
            }

            $vehicle = $this->data($vehicle);

            return response()->json([
                'success' => true,
                'vehicle' => $vehicle
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'number_plate' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            try {
                $vehicle = Vehicle::find($id);
                if (!$vehicle) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Vehicle not found'
                    ], 500);
                }
                    
                $vehicle->update([
                    'number_plate' => $request->input('number_plate')
                ]);

                return response()->json([
                    'success' => true,
                    'message' => 'Update vehicle successful'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            try {
                $vehicle = Vehicle::find($id);
                if (!$vehicle) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Vehicle not found'
                    ], 500);
                }
                    
                $vehicle->delete();

                return response()->json([
                    'success' => true,
                    'message' => 'Delete vehicle successful'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function multipleDestroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $ids = explode(",", $request->input('ids'));
            foreach ($ids as $id) {
                $vehicle = Vehicle::find($id);
                if ($vehicle) $vehicle->delete();
            }

            return response()->json([
                'success' => true,
                'message' => 'Delete vehicles successful'
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    // ============================================ Private Fuction ============================================ //

    private function dataArray($vehicles)
    {
        $data = [];
        foreach ($vehicles as $vehicle) {
            $data[] = [
                'id' => $vehicle->id,
                'vehicle_id' => $vehicle->vehicle_id,
                'number_plate' => $vehicle->number_plate
            ];
        }

        return $data;
    }

    private function data($vehicle)
    {
        return [
            'id' => $vehicle->id,
            'vehicle_id' => $vehicle->vehicle_id,
            'number_plate' => $vehicle->number_plate
        ];
    }
}
