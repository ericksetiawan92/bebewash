<?php

namespace App\Http\Controllers\Api;

use App\Utils\CodeFactory;
use Validator;
use App\Agent;
use App\Token;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
        	'token'    => 'required'
        ]);

        if ($validator->fails()) {
        	return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
        			->first();

        if ($token) {
        	$all = Agent::select(['id', 'agent_id', 'name', 'email']);

            return DataTables::of($all)
                ->addColumn('check', function ($obj) {
                    return '
                    <input type="checkbox" name=id[] value='.$obj->id.'>
                ';
                })
                ->editColumn('agent_id', function (Agent $obj) {
                    return $obj->agent_id;
                })
                ->addColumn('name', function (Agent $obj) {
                    return $obj->name;
                })
                ->addColumn('email', function (Agent $obj) {
                    return $obj->email;
                })
                ->addColumn('total_order', function (Agent $obj) {
                    return 0;
                })
                ->addColumn('actions', function ($obj) {
                    return '
                    <a class="mr-3" href="'.route("master.agents.edit", $obj->id).'"><img src="'.url("public/images/icons/edit.svg").'" alt="edit" width="16"></a>
                    <a href="javascript:void(0)" data-id="'.$obj->id.'" class="action-del"><img src="'.url("public/images/icons/trash.svg").'" alt="trash" width="16"></a>
                ';
                })
                ->rawColumns(['check','actions'])
                ->make(true);
        }

        return response()->json([
        	'success' => false,
        	'message' => 'Token not available'
        ], 500);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            return response()->json([
                'success' => true,
                'new_agent_id' => CodeFactory::simpleCode(Agent::class, 'AG')
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'new_agent_id' => 'required',
            'name' => 'required',
            'group' => 'required|in:DIRECT',
            'email' => 'required',
            'address' => 'required',
            'contact_person_name' => 'required',
            'contact_person_phone_number' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            try {
                $agent = Agent::create([
                    'agent_id' => CodeFactory::simpleCode(Agent::class, 'AG'),
                    'name' => $request->input('name'),
                    'group' => $request->input('group'),
                    'phone_number' => $request->input('phone_number'),
                    'mobile_phone_number' => $request->input('mobile_phone_number'),
                    'email' => $request->input('email'),
                    'address' => $request->input('address'),
                    'city' => $request->input('city'),
                    'country' => $request->input('country'),
                    'kecamatan' => $request->input('kecamatan'),
                    'kelurahan' => $request->input('kelurahan'),
                    'contact_person_name' => $request->input('contact_person_name'),
                    'contact_person_phone_number' => $request->input('contact_person_phone_number'),
                    'contact_person_mobile_phone_number' => $request->input('contact_person_mobile_phone_number')
                ]);

                return response()->json([
                    'success' => true,
                    'message' => 'Create agent successful'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $agent = Agent::find($id);
            if (!$agent) {
                return response()->json([
                    'success' => false,
                    'message' => 'Agent not found'
                ], 500);
            }

            $price = $this->data($agent);

            return response()->json([
                'success' => true,
                'agent' => $agent
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'name' => 'required',
            'group' => 'required|in:DIRECT',
            'email' => 'required',
            'address' => 'required',
            'contact_person_name' => 'required',
            'contact_person_phone_number' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            try {
                $agent = Agent::find($id);
                if (!$agent) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Agent not found'
                    ], 500);
                }
                    
                $agent->update([
                    'name' => $request->input('name'),
                    'group' => $request->input('group'),
                    'phone_number' => $request->input('phone_number'),
                    'mobile_phone_number' => $request->input('mobile_phone_number'),
                    'email' => $request->input('email'),
                    'address' => $request->input('address'),
                    'city' => $request->input('city'),
                    'country' => $request->input('country'),
                    'kecamatan' => $request->input('kecamatan'),
                    'kelurahan' => $request->input('kelurahan'),
                    'contact_person_name' => $request->input('contact_person_name'),
                    'contact_person_phone_number' => $request->input('contact_person_phone_number'),
                    'contact_person_mobile_phone_number' => $request->input('contact_person_mobile_phone_number')
                ]);

                return response()->json([
                    'success' => true,
                    'message' => 'Update agent successful'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            try {
                $agent = Agent::find($id);
                if (!$agent) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Agent not found'
                    ], 500);
                }
                    
                $agent->delete();

                return response()->json([
                    'success' => true,
                    'message' => 'Delete agent successful'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function multipleDestroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $ids = explode(",", $request->input('ids'));
            foreach ($ids as $id) {
                $agent = Agent::find($id);
                if ($agent) $agent->delete();
            }

            return response()->json([
                'success' => true,
                'message' => 'Delete agents successful'
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    // ============================================ Private Fuction ============================================ //

    private function dataArray($agents)
    {
        $data = [];
        foreach ($agents as $agent) {
            $data[] = [
                'id' => $agent->id,
                'agent_id' => $agent->agent_id,
                'name' => $agent->name,
                'group' => $agent->group,
                'phone_number' => $agent->phone_number,
                'mobile_phone_number' => $agent->mobile_phone_number,
                'email' => $agent->email,
                'address' => $agent->address,
                'city' => $agent->city,
                'country' => $agent->country,
                'kecamatan' => $agent->kecamatan,
                'kelurahan' => $agent->kelurahan,
                'contact_person_name' => $agent->contact_person_name,
                'contact_person_phone_number' => $agent->contact_person_phone_number,
                'contact_person_mobile_phone_number' => $agent->contact_person_mobile_phone_number
            ];
        }

        return $data;
    }

    private function data($agent)
    {
        return [
            'id' => $agent->id,
            'agent_id' => $agent->agent_id,
            'name' => $agent->name,
            'group' => $agent->group,
            'phone_number' => $agent->phone_number,
            'mobile_phone_number' => $agent->mobile_phone_number,
            'email' => $agent->email,
            'address' => $agent->address,
            'city' => $agent->city,
            'country' => $agent->country,
            'kecamatan' => $agent->kecamatan,
            'kelurahan' => $agent->kelurahan,
            'contact_person_name' => $agent->contact_person_name,
            'contact_person_phone_number' => $agent->contact_person_phone_number,
            'contact_person_mobile_phone_number' => $agent->contact_person_mobile_phone_number
        ];
    }
}
