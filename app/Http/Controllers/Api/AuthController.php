<?php

namespace App\Http\Controllers\Api;

use File;
use Hash;
use Auth;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use App\User;
use App\Token;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
        	'email' => 'required',
        	'password' => 'required'
        ]);

        if ($validator->fails()) {
        	return response()->json(['errors' => $validator->errors()], 422);
        }


        $user = User::where('email', $request->input('email'))->first();

        if ($user) {
            if (!Hash::check($request->input('password'), $user->password)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Wrong password',
                ], 500);
            }
        	$token = Token::where('user_id', $user->id)->first();

        	if (!$token) {
        		$tokens = Token::where('user_id', $user->id)
                            ->get();
        		foreach ($tokens as $token) {
        			$token->delete();
        		}

        		$token = Token::create([
        			'user_id' 	 => $user->id,
        			'token'      => Hash::make(Carbon::now())
        		]);
        	}

        	return response()->json([
	        	'success' => true,
	        	'message' => 'Login successful',
	        	'token'   => $token->token
	        ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'User not found'
        ], 500);
    }

    public function logout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $token->delete();

            return redirect('login');
        }

        return redirect('login');
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
        	'email' => 'required'
        ]);

        if ($validator->fails()) {
        	return response()->json(['errors' => $validator->errors()], 422);
        }

        $user = User::where('email', $request->input('email'))->first();

        if ($user) {
            $password = $this->generateRandomPassword();
            $user->update([
                'password' => Hash::make($password)
            ]);

        	$this->sendEmail('FORGOT-PASSWORD', [$request->input('email')], ['new_password' => $password]);

        	return response()->json([
        		'success' => true,
        		'message' => 'Password changed and sent to email',
        	]);
        }
        
        return response()->json([
        	'success' => false,
        	'message' => 'Email not available'
        ], 500);
    }

    private function generateRandomPassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = [];

        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        return implode($pass); 
    }

    // ============================================ Use Token ============================================ //


    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $user = $token->user()->first();
            $user->update([
                'password' => Hash::make($request->input('password'))
            ]);

            $this->sendEmail('CHANGE-PASSWORD', [$user->email], ['new_password' => $request->input('password')]);

            return response()->json([
                'success' => true,
                'message' => 'Change password successful'
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    public function authenticated(Request $request)
    {
    	$validator = Validator::make($request->all(), [
        	'token'    => 'required'
        ]);

        if ($validator->fails()) {
        	return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
        			->first();

        if ($token) {
        	$user = $token->user()->first();

        	if (!$user) {
        		return response()->json([
		        	'success' => false,
		        	'message' => 'User not available'
		        ], 500);
        	}

            $userProfile = $this->userProfile($user);

        	return response()->json([
	        	'success' => true,
	        	'message' => 'Authentication successful',
                'user' => $userProfile
	        ]);
        }

        return response()->json([
        	'success' => false,
        	'message' => 'Token not available'
        ], 500);
    }

    private function userProfile($user)
    {
        return [
            'id' => $user->id,
            'name'  => $user->name,
            'email' => $user->email,
            'role' => $user->role
        ];
    }
}
