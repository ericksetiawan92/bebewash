<?php

namespace App\Http\Controllers\Api;

use App\Utils\CodeFactory;
use Validator;
use App\Item;
use App\Price;
use App\Token;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
        	'token'    => 'required'
        ]);

        if ($validator->fails()) {
        	return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
        			->first();

        if ($token) {
            $all = Item::select('id','item_id','group','description','subcategory', 'price_id');

            return DataTables::of($all)
                ->addColumn('check', function ($obj) {
                    return '
                    <input type="checkbox" name=id[] value='.$obj->id.'>
                ';
                })
                ->editColumn('price_id', function (Item $obj) {
                    return optional($obj->price)->price;
                })
                ->addColumn('actions', function ($obj) {
                    return '
                    <a class="mr-3" href="'.route("master.items.edit", $obj->id).'"><img src="'.url("public/images/icons/edit.svg").'" alt="edit" width="16"></a>
                    <a href="javascript:void(0)" data-id="'.$obj->id.'" class="action-del"><img src="'.url("public/images/icons/trash.svg").'" alt="trash" width="16"></a>
                ';
                })
                ->rawColumns(['check','actions'])
                ->make(true);

        	return response()->json([
	        	'success' => true,
                'items' => $items,
                'count' => $count
	        ]);
        }

        return response()->json([
        	'success' => false,
        	'message' => 'Token not available'
        ], 500);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            return response()->json([
                'success' => true,
                'new_item_id' => CodeFactory::simpleCode(Item::class, 'IT')
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'price_id' => 'required|numeric',
            'type' => 'required|in:SERVICE,ITEM',
            'group' => 'required|in:BUBBLE_WASH,SCRUB_WASH,SQUEEKLY_CLEAN_WASH',
            'subcategory' => 'required|in:SINGLE_SEAT_STROLLER,DOUBLE_SEAT_STROLLER',
            'name_of_product' => 'required_if:type,SERVICE',
            'name_of_service' => 'required_if:type,SERVICE'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $price = Price::find($request->input('price_id'));
            if ($price) {
                try {
                    $item = Item::create([
                        'item_id' => CodeFactory::simpleCode(Item::class, 'IT'),
                        'description' => $request->input('description'),
                        'price_id' => $request->input('price_id'),
                        'type' => $request->input('type'),
                        'group' => $request->input('group'),
                        'subcategory' => $request->input('subcategory'),
                        'name_of_service' => $request->input('name_of_service'),
                        'name_of_product' => $request->input('name_of_product')
                    ]);

                    return response()->json([
                        'success' => true,
                        'message' => 'Create item successful',
                        'next_code' => CodeFactory::simpleCode(Item::class, 'IT')
                    ]);
                } catch (\Exception $e) {
                    return response()->json([
                        'success' => false,
                        'message' => $e->getMessage()
                    ], 500);
                }
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Price not found'
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $item = Item::find($id);
            if (!$item) {
                return response()->json([
                    'success' => false,
                    'message' => 'Item not found'
                ], 500);
            }

            $item = $this->data($item);

            return response()->json([
                'success' => true,
                'item' => $item
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'price_id' => 'required|numeric',
            'type' => 'required|in:SERVICE,ITEM',
            'group' => 'required|in:BUBBLE_WASH,SCRUB_WASH,SQUEEKLY_CLEAN_WASH',
            'subcategory' => 'required|in:SINGLE_SEAT_STROLLER,DOUBLE_SEAT_STROLLER',
            'name_of_product' => 'required_if:type,SERVICE',
            'name_of_service' => 'required_if:type,SERVICE'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $price = Price::find($request->input('price_id'));
            if ($price) {
                try {
                    $item = Item::find($id);
                    if (!$item) {
                        return response()->json([
                            'success' => false,
                            'message' => 'Item not found'
                        ], 500);
                    }
                    
                    $item->update([
                        'description' => $request->input('description'),
                        'price_id' => $request->input('price_id'),
                        'type' => $request->input('type'),
                        'group' => $request->input('group'),
                        'subcategory' => $request->input('subcategory'),
                        'name_of_service' => $request->input('name_of_service'),
                        'name_of_product' => $request->input('name_of_product')
                    ]);

                    return response()->json([
                        'success' => true,
                        'message' => 'Update item successful'
                    ]);
                } catch (\Exception $e) {
                    return response()->json([
                        'success' => false,
                        'message' => $e->getMessage()
                    ], 500);
                }
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Item not found'
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            try {
                $item = Item::find($id);
                if (!$item) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Item not found'
                    ], 500);
                }
                    
                $item->delete();

                return response()->json([
                    'success' => true,
                    'message' => 'Delete item successful'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function multipleDestroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $ids = explode(",", $request->input('ids'));
            foreach ($ids as $id) {
                $item = Item::find($id);
                if ($item) $item->delete();
            }

            return response()->json([
                'success' => true,
                'message' => 'Delete items successful'
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    // ============================================ Private Fuction ============================================ //

    private function dataArray($items)
    {
        $data = [];
        foreach ($items as $item) {
            $data[] = [
                'id' => $item->id,
                'item_id' => $item->item_id,
                'type' => $item->type,
                'description' => $item->description,
                'group' => $item->group,
                'subcategory' => $item->subcategory,
                'price' => [
                    'id' => $item->price->id,
                    'name' => $item->price->name,
                    'price' => $item->price->price
                ],
                'name_of_product' => $item->name_of_product,
                'name_of_service' => $item->name_of_service
            ];
        }

        return $data;
    }

    private function data($item)
    {
        return [
            'id' => $item->id,
            'item_id' => $item->item_id,
            'type' => $item->type,
            'description' => $item->description,
            'group' => $item->group,
            'subcategory' => $item->subcategory,
            'price' => [
                'id' => $item->price->id,
                'name' => $item->price->name,
                'price' => $item->price->price
            ],
            'name_of_product' => $item->name_of_product,
            'name_of_service' => $item->name_of_service
        ];
    }
}
