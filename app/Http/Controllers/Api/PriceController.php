<?php

namespace App\Http\Controllers\Api;

use Validator;
use App\Price;
use App\Token;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
        	'token'    => 'required'
        ]);

        if ($validator->fails()) {
        	return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
        			->first();

        if ($token) {
        	$user = $token->user()->first();

            $start = ($request->input('start')) ? $request->input('start') : 0;
            $limit = ($request->input('limit')) ? $request->input('limit') : 50;

            $count = Price::count();
            $prices = Price::skip($start)->take($limit)->orderBy('id', 'DESC')->get();
            $prices = $this->dataArray($prices);

        	return response()->json([
	        	'success' => true,
                'prices' => $prices,
                'count' => $count
	        ]);
        }

        return response()->json([
        	'success' => false,
        	'message' => 'Token not available'
        ], 500);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'name' => 'required',
            'price' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            try {
                $price = Price::create([
                    'price' => $request->input('price'),
                    'name' => $request->input('name')
                ]);

                return response()->json([
                    'success' => true,
                    'message' => 'Create price successful'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $price = Price::find($id);
            if (!$price) {
                return response()->json([
                    'success' => false,
                    'message' => 'Price not found'
                ], 500);
            }

            $price = $this->data($price);

            return response()->json([
                'success' => true,
                'price' => $price
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'name' => 'required',
            'price' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            try {
                $price = Price::find($id);
                if (!$price) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Price not found'
                    ], 500);
                }
                    
                $price->update([
                    'name' => $request->input('name'),
                    'price' => $request->input('price')
                ]);

                return response()->json([
                    'success' => true,
                    'message' => 'Update price successful'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            try {
                $price = Price::find($id);
                if (!$price) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Price not found'
                    ], 500);
                }
                    
                $price->delete();

                return response()->json([
                    'success' => true,
                    'message' => 'Delete price successful'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function multipleDestroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            foreach ($request->input('ids') as $id) {
                $price = Price::find($id);
                if ($price) $price->delete();
            }

            return response()->json([
                'success' => true,
                'message' => 'Delete prices successful'
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    // ============================================ Private Fuction ============================================ //

    private function dataArray($prices)
    {
        $data = [];
        foreach ($prices as $price) {
            $data[] = [
                'id' => $price->id,
                'name' => $price->name,
                'price' => $price->price
            ];
        }

        return $data;
    }

    private function data($price)
    {
        return [
            'id' => $price->id,
            'name' => $price->name,
            'price' => $price->price
        ];
    }
}
