<?php

namespace App\Http\Controllers\Api;

use App\Utils\CodeFactory;
use Validator;
use App\Customer;
use App\Token;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
        	'token'    => 'required'
        ]);

        if ($validator->fails()) {
        	return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
        			->first();

        if ($token) {

            $all = Customer::select('id','customer_id', 'name','city','created_at');

            return DataTables::of($all)
                ->addColumn('check', function ($obj) {
                    return '
                    <input type="checkbox" name=id[] value='.$obj->id.'>
                ';
                })
                ->editColumn('created_at', function ($obj) {
                    return date("d/m/Y", strtotime($obj->created_at));
                })
                ->addColumn('actions', function ($obj) {
                    return '
                    <a class="mr-3" href="'.route("master.customers.edit", $obj->id).'"><img src="'.url("public/images/icons/edit.svg").'" alt="edit" width="16"></a>
                    <a href="javascript:void(0)" data-id="'.$obj->id.'" class="action-del"><img src="'.url("public/images/icons/trash.svg").'" alt="trash" width="16"></a>
                ';
                })
                ->rawColumns(['check','actions'])
                ->make(true);

        	return response()->json([
	        	'success' => true,
                'customers' => $customers,
                'count' => $count
	        ]);
        }

        return response()->json([
        	'success' => false,
        	'message' => 'Token not available'
        ], 500);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $code = CodeFactory::simpleCode(Customer::class, 'CU');

            return response()->json([
                'success' => true,
                'new_customer_id' => $code
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'partner_type' => 'required|in:CUSTOMER,VENDOR,ENDORSER',
            'name' => 'required',
            'email' => 'required',
            'bebe_name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'check_same_address' => 'required|boolean'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            try {
                $customer = Customer::create([
                    'customer_id' => CodeFactory::simpleCode(Customer::class,'CU'),
                    'partner_type' => $request->input('partner_type'),
                    'name' => $request->input('name'),
                    'birthday' => Carbon::createFromFormat("d F Y",$request->input('birthday')),
                    'religion' => $request->input('religion'),
                    'gender' => $request->input('gender'),
                    'phone_number' => $request->input('phone_number'),
                    'email' => $request->input('email'),
                    'bebe_name' => $request->input('bebe_name'),
                    'bebe_birthday' => $request->input('bebe_birthday'),
                    'bebe_gender' => $request->input('bebe_gender'),
                    'address' => $request->input('address'),
                    'city' => $request->input('city'),
                    'country' => $request->input('country'),
                    'kecamatan' => $request->input('kecamatan'),
                    'kelurahan' => $request->input('kelurahan'),
                    'check_same_address' => $request->input('check_same_address'),
                    'shipping_address' => ($request->input('check_same_address')) ? null : $request->input('shipping_address'),
                    'shipping_city' => ($request->input('check_same_address')) ? null : $request->input('shipping_city'),
                    'shipping_country' => ($request->input('check_same_address')) ? null : $request->input('shipping_country'),
                    'shipping_kecamatan' => ($request->input('check_same_address')) ? null : $request->input('shipping_kecamatan'),
                    'shipping_kelurahan' => ($request->input('check_same_address')) ? null : $request->input('shipping_kelurahan')
                ]);

                return response()->json([
                    'success' => true,
                    'message' => 'Create customer successful'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $customer = Customer::find($id);
            if (!$customer) {
                return response()->json([
                    'success' => false,
                    'message' => 'Customer not found'
                ], 500);
            }

            $price = $this->data($customer);

            return response()->json([
                'success' => true,
                'customer' => $customer
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'partner_type' => 'required|in:CUSTOMER,VENDOR,ENDORSER',
            'name' => 'required',
            'email' => 'required',
            'bebe_name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'check_same_address' => 'required|boolean'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            try {
                $customer = Customer::find($id);
                if (!$customer) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Customer not found'
                    ], 500);
                }
                    
                $customer->update([
                    'partner_type' => $request->input('partner_type'),
                    'name' => $request->input('name'),
                    'birthday' => $request->input('birthday'),
                    'religion' => $request->input('religion'),
                    'gender' => $request->input('gender'),
                    'phone_number' => $request->input('phone_number'),
                    'email' => $request->input('email'),
                    'bebe_name' => $request->input('bebe_name'),
                    'bebe_birthday' => $request->input('bebe_birthday'),
                    'bebe_gender' => $request->input('bebe_gender'),
                    'address' => $request->input('address'),
                    'city' => $request->input('city'),
                    'country' => $request->input('country'),
                    'kecamatan' => $request->input('kecamatan'),
                    'kelurahan' => $request->input('kelurahan'),
                    'check_same_address' => $request->input('check_same_address'),
                    'shipping_address' => ($request->input('check_same_address')) ? null : $request->input('shipping_address'),
                    'shipping_city' => ($request->input('check_same_address')) ? null : $request->input('shipping_city'),
                    'shipping_country' => ($request->input('check_same_address')) ? null : $request->input('shipping_country'),
                    'shipping_kecamatan' => ($request->input('check_same_address')) ? null : $request->input('shipping_kecamatan'),
                    'shipping_kelurahan' => ($request->input('check_same_address')) ? null : $request->input('shipping_kelurahan')
                ]);

                return response()->json([
                    'success' => true,
                    'message' => 'Update customer successful'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            try {
                $customer = Customer::find($id);
                if (!$customer) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Customer not found'
                    ], 500);
                }
                    
                $customer->delete();

                return response()->json([
                    'success' => true,
                    'message' => 'Delete customer successful'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function multipleDestroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $token = Token::where('token', $request->input('token'))
                    ->first();
        if ($token) {
            $ids = explode(",", $request->input('ids'));
            foreach ($ids as $id) {
                $customer = Customer::find($id);
                if ($customer) $customer->delete();
            }

            return response()->json([
                'success' => true,
                'message' => 'Delete customers successful'
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Token not available'
        ], 500);
    }

    // ============================================ Private Fuction ============================================ //

    private function dataArray($customers)
    {
        $data = [];
        foreach ($customers as $customer) {
            $data[] = [
                'id' => $customer->id,
                'customer_id' => $customer->customer_id,
                'partner_type' => $customer->partner_type,
                'name' => $customer->name,
                'birthday' => $customer->birthday,
                'religion' => $customer->religion,
                'gender' => $customer->gender,
                'phone_number' => $customer->phone_number,
                'email' => $customer->email,
                'bebe_name' => $customer->bebe_name,
                'bebe_birthday' => $customer->bebe_birthday,
                'bebe_gender' => $customer->bebe_gender,
                'address' => $customer->address,
                'city' => $customer->city,
                'country' => $customer->country,
                'kecamatan' => $customer->kecamatan,
                'kelurahan' => $customer->kelurahan,
                'check_same_address' => $customer->check_same_address,
                'shipping_address' => $customer->shipping_address,
                'shipping_city' => $customer->shipping_city,
                'shipping_country' => $customer->shipping_country,
                'shipping_kecamatan' => $customer->shipping_kecamatan,
                'shipping_kelurahan' => $customer->shipping_kelurahan
            ];
        }

        return $data;
    }

    private function data($customer)
    {
        return [
            'id' => $customer->id,
            'customer_id' => $customer->customer_id,
            'partner_type' => $customer->partner_type,
            'name' => $customer->name,
            'birthday' => $customer->birthday,
            'religion' => $customer->religion,
            'gender' => $customer->gender,
            'phone_number' => $customer->phone_number,
            'email' => $customer->email,
            'bebe_name' => $customer->bebe_name,
            'bebe_birthday' => $customer->bebe_birthday,
            'bebe_gender' => $customer->bebe_gender,
            'address' => $customer->address,
            'city' => $customer->city,
            'country' => $customer->country,
            'kecamatan' => $customer->kecamatan,
            'kelurahan' => $customer->kelurahan,
            'check_same_address' => $customer->check_same_address,
            'shipping_address' => $customer->shipping_address,
            'shipping_city' => $customer->shipping_city,
            'shipping_country' => $customer->shipping_country,
            'shipping_kecamatan' => $customer->shipping_kecamatan,
            'shipping_kelurahan' => $customer->shipping_kelurahan
        ];
    }
}
