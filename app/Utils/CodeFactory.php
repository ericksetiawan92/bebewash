<?php

namespace App\Utils;


class CodeFactory
{
    public static function simpleCode($CLASS, $code) {
        $count = $CLASS::max('id');
        return $code . sprintf('%04d', $count + 1);
    }

}
