<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends Model
{
    use SoftDeletes;
    
    protected $guarded = [];

    public function items()
    {
        return $this->hasMany('App\Item');
    }
}
