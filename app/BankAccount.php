<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccount extends Model
{
    use SoftDeletes;
    
    protected $guarded = [];

    public function bank()
    {
        return $this->belongsTo('App\Bank');
    }
}
