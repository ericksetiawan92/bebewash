<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Bank;
use Carbon\Carbon;

class MasterBankSeeder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'master-bank-seeder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Master Bank Seeder';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $banks = [
            'BCA', 'Mandiri'
        ];

        foreach ($banks as $bank) {
            Bank::create([
                'name' => $bank
            ]);
        }

        $this->info('Master bank already inserted!');
        
    }
}