<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Hash;
use Carbon\Carbon;

class UserSeeder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user-seeder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'User Seeder';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        User::create([
            'email' => 'admin@admin.com',
            'password' => Hash::make('test123'),
            'role' => 'superadmin',
            'name' => 'Super Administrator'
        ]);

        $this->info('User administrator already inserted!');
        
    }
}