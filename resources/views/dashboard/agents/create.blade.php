@extends('layouts.dashboard')

@section('content')
    <div class="c-title row no-gutters">
        <div class="col-sm-6">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb c-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('master.agents.index') }}">Agent data</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add agent</li>
                </ol>
            </nav>
            <h1 class="mb-0">Add agent data</h1>
        </div>
        <div class="col-sm-6 text-right"></div>
    </div>
    <div class="alert" id="alert"></div>
    <form class="c-form" novalidate="">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="c-form--title">Agent Data</h2>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="group">Agent group</label>
                            <select class="form-control" id="group" required="">
                                <option value="DIRECT">Direct</option>
                            </select>
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="agent-id">Agent ID</label>
                            <input class="form-control" id="agent-id" value="" readonly="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="c-form--label" for="name">Name</label>
                    <input class="form-control" id="name" value="" required="">
                    <div class="invalid-feedback">Data invalid.</div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="phone-number">Telephone</label>
                            <input class="form-control" id="phone-number" value="" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="mobile-phone-number">Mobile Phone</label>
                            <input class="form-control" id="mobile-phone-number" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="c-form--label" for="email">Email</label>
                    <input class="form-control" id="email" value="" required="">
                    <div class="invalid-feedback">Data invalid.</div>
                </div>
            </div>
            <div class="col-sm-6">
                <h2 class="c-form--title">Orders</h2>
                <div class="form-group">
                    <label class="c-form--label" for="total-orders">Total Orders</label>
                    <input class="form-control" id="total-orders" value="0" readonly="">
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-sm-6">
                <h2 class="c-form--title">Address</h2>
                <div class="form-group">
                    <label class="c-form--label" for="address">Address</label>
                    <textarea class="form-control" id="address"></textarea>
                    <div class="invalid-feedback">Data invalid.</div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="country">Country</label>
                            <input class="form-control" id="country" value="" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="city">City</label>
                            <input class="form-control" id="city" value="" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="kecamatan">Kecamatan</label>
                            <input class="form-control" id="kecamatan" value="" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="kelurahan">Kelurahan</label>
                            <input class="form-control" id="kelurahan" value="" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <h2 class="c-form--title">Contact Person</h2>
                <div class="form-group">
                    <label class="c-form--label" for="contact-person-name">Name</label>
                    <input class="form-control" id="contact-person-name" value="" required="">
                    <div class="invalid-feedback">Data invalid.</div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="contact-person-phone-number">Telephone</label>
                            <input class="form-control" id="contact-person-phone-number" value="" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="contact-person-mobile-phone-number">Mobile Phone</label>
                            <input class="form-control" id="contact-person-mobile-phone-number" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="my-4">
        <div class="text-right">
            <a href="{{ route('master.agents.index')}}" class="btn btn-light mr-2" type="button">Cancel</a>
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </form>
@stop
@push('scripts')
<script>
    function firstLoad(token) {
        var token = getCookie('token');
        $.ajax({
            type: 'GET',
            url: '{{url('api/bebewash/agents/create')}}',
            data: 'token=' + token ,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (msg) {
                if (msg.success) {
                    $("#agent-id").val(msg.new_agent_id)
                }
            },
            error: function(msg) {
                console.log(msg.responseJSON.message)
            }
        });
    }

    $(document).ready(function() {
        firstLoad();
    });

    $("form").submit(function() {
        $("button[type=submit]").hide();
        $.ajax({
            type: 'POST',
            url: '{{url('api/bebewash/agents')}}',
            data: '{ "token":"' + getCookie("token") + '",' +
            '"group":"' + $("#group").val() + '",' +
            '"new_agent_id":"' + $("#agent-id").val() + '",' +
            '"name":"' + $("#name").val() + '",' +
            '"phone_number":"' + $("#phone-number").val() + '",' +
            '"mobile_phone_number":"' + $("#mobile-phone-number").val() + '",' +
            '"email":"' + $("#email").val() + '",' +
            '"address":"' + $("#address").val() + '",' +
            '"city":"' + $("#city").val() + '",' +
            '"country":"' + $("#country").val() + '",' +
            '"kecamatan":"' + $("#kecamatan").val() + '",' +
            '"kelurahan":"' + $("#kelurahan").val() + '",' +
            '"contact_person_name":"' + $("#contact-person-name").val() + '",' +
            '"contact_person_phone_number":"' + $("#contact-person-phone-number").val() + '",' +
            '"contact_person_mobile_phone_number":"' + $("#contact-person-mobile-phone-number").val() + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (msg) {
                if (msg.success) {
                    $("#alert").addClass('alert-success');
                    $("#alert").removeClass('alert-danger');
                    $("#alert").html("Agent inserted");
                    $("button[type=submit]").show();
                    $('form').trigger("reset");
                    firstLoad();
                }
            },
            error: function(msg) {
                $("button[type=submit]").show();
                var errors = $.parseJSON(msg.responseText);

                if (errors.errors) {
                    errors = errors.errors;
                    $("#alert").html("");
                    for (var key in errors) {
                        $("#alert").append(errors[key] + "<br/>");
                    }
                    $("#alert").removeClass('alert-success');
                    $("#alert").addClass('alert-danger');
                    firstLoad();
                }
            }
        });
       return false;
    });
</script>
@endpush