@extends('layouts.dashboard')

@section('content')
    <div class="c-title row no-gutters">
        <div class="col-sm-6">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb c-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('master.vehicles.index') }}">Vehicle data</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add vehicle</li>
                </ol>
            </nav>
            <h1 class="mb-0">Add vehicle data</h1>
        </div>
        <div class="col-sm-6 text-right"></div>
    </div>
    <div class="alert" id="alert"></div>
    <form class="c-form" novalidate="">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="c-form--title">Vehicle Data</h2>
                    <div class="form-group">
                        <label class="c-form--label" for="vehicle-id">Vehicle ID</label>
                        <input class="form-control" id="vehicle-id" value="" readonly="">
                        <div class="invalid-feedback">Data invalid.</div>
                    </div>
                    <div class="form-group">
                        <label class="c-form--label" for="number-plate">Number plate</label>
                        <input class="form-control" id="number-plate" required="">
                        <div class="invalid-feedback">Data invalid.</div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="my-4">
        <div class="text-right">
            <a href="{{ route('master.vehicles.index')}}" class="btn btn-light mr-2" type="button">Cancel</a>
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </form>
@stop
@push('scripts')
<script>
    function firstLoad(token) {
        var token = getCookie('token');
        $.ajax({
            type: 'GET',
            url: '{{url('api/bebewash/vehicles/create')}}',
            data: 'token=' + token ,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (msg) {
                if (msg.success) {
                    $("#vehicle-id").val(msg.new_vehicle_id)
                }
            },
            error: function(msg) {
                console.log(msg.responseJSON.message)
            }
        });
    }

    $(document).ready(function() {
        firstLoad();
    });

    $("form").submit(function() {
        $("button[type=submit]").hide();
        $.ajax({
            type: 'POST',
            url: '{{url('api/bebewash/vehicles')}}',
            data: '{ "token":"' + getCookie("token") + '",' +
            '"new_vehicle_id":"' + $("#vehicle-id").val() + '",' +
            '"number_plate":"' + $("#number-plate").val() + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (msg) {
                if (msg.success) {
                    $("#alert").addClass('alert-success');
                    $("#alert").removeClass('alert-danger');
                    $("#alert").html("Vehicle inserted");
                    $("button[type=submit]").show();
                    $('form').trigger("reset");
                    firstLoad();
                }
            },
            error: function(msg) {
                $("button[type=submit]").show();
                var errors = $.parseJSON(msg.responseText);

                if (errors.errors) {
                    errors = errors.errors;
                    $("#alert").html("");
                    for (var key in errors) {
                        $("#alert").append(errors[key] + "<br/>");
                    }
                    $("#alert").removeClass('alert-success');
                    $("#alert").addClass('alert-danger');
                    firstLoad();
                }
            }
        });
       return false;
    });
</script>
@endpush