@extends('layouts.dashboard')

@section('content')
    <div class="c-title row no-gutters">
        <div class="col-sm-6">
            <h1 class="mb-0">Item data</h1>
        </div>
        <div class="col-sm-6 text-right"><a class="btn btn-primary" href="{{ route('master.items.create') }}">Add item data</a></div>
    </div>
    <div id="alert" class="alert"></div>
    <div class="c-table--outer">
        <div class="c-title">
            <table class="c-table table table-striped" id="dataTable">
                <thead>
                <tr>
                    <th class="checkbox">
                        <input type="checkbox" id="checkAll">
                    </th>
                    <th>ID</th>
                    <th>Description</th>
                    <th>Item group</th>
                    <th>Sub category</th>
                    <th>Price</th>
                    <th></th>
                </tr>
                </thead>
            </table>
            <a class="btn btn-primary" id="delete-selected" href="">Delete selected</a>
        </div>
    </div>
@stop

@include('partials.datatable')
@push('scripts')
<div class="modal" id="confirm" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Confirmation</h3>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><img src="{{ url('public/images/icons/x.svg') }}" alt="close"></button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this data?</p>
            </div>
            <div class="modal-footer text-right">
                <input type="hidden" name="delete_id">
                <button class="btn btn-light mr-2" type="button" data-dismiss="modal">Cancel</button>
                <button class="btn btn-danger" id="submit-delete" type="submit">Delete</button>
            </div>
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function() {
        var token = getCookie('token');
        table = $('#dataTable').DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": "{{ url('api/bebewash/items') }}?token="+token,
            columns: [
                { data: 'check', name: 'check', searchable: false, sortable: false },
                { data: 'item_id', name: 'item_id' },
                { data: 'description', name: 'description' },
                { data: 'group', name: 'group' },
                { data: 'subcategory', name: 'subcategory' },
                { data: 'price_id', name: 'price_id' },
                { data: 'actions', name: 'actions', searchable: false, sortable: false }
            ]
        } );
    } );

    $("body").on('click', '.action-del', function() {
        $("input[name=delete_id]").val($(this).attr('data-id'));
       $("#confirm").modal();
    });

    $("#submit-delete").click(function() {
        var id=$("input[name=delete_id]").val();
        $(this).hide();
        $.ajax({
            type: 'DELETE',
            url: '{{url('api/bebewash/items/')}}/'+id,
            data: '{ "token":"' + getCookie("token") + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (msg) {
                if (msg.success) {
                    $("#alert").addClass('alert-danger');
                    $("#alert").html("Item deleted");
                    $("#submit-delete").show();
                    $("#confirm").modal('hide');
                    table.ajax.reload(null, false);
                }
            },
            error: function(msg) {
                $("#submit-delete").show();
                var errors = $.parseJSON(msg.responseText);

                if (errors.errors) {
                    errors = errors.errors;
                    $("#alert").html("");
                    for (var key in errors) {
                        $("#alert").append(errors[key] + "<br/>");
                    }
                    $("#alert").removeClass('alert-success');
                    $("#alert").addClass('alert-danger');
                }
            }
        });
    });

    $("#checkAll").click(function() {
        if($(this).prop('checked'))
            $("input[name^=id]").prop('checked',true);
        else
            $("input[name^=id]").prop('checked',false);
    });

    $("#delete-selected").click(function() {
        event.preventDefault();
        var idArr = [];
        $("input[name^=id]").each(function() {
            if($(this).prop('checked'))
                idArr.push($(this).val());
        });

        $.ajax({
            type: 'POST',
            url: '{{url('api/bebewash/items/multiple-delete')}}',
            data: '{ "token":"' + getCookie("token") + '", "ids":"'+ idArr +'"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (msg) {
                if (msg.success) {
                    $("#alert").addClass('alert-danger');
                    $("#alert").html("Item(s) deleted");
                    $("#submit-delete").show();
                    $("#confirm").modal('hide');
                    table.ajax.reload(null, false);
                }
            },
            error: function(msg) {
                $("#submit-delete").show();
                var errors = $.parseJSON(msg.responseText);

                if (errors.errors) {
                    errors = errors.errors;
                    $("#alert").html("");
                    for (var key in errors) {
                        $("#alert").append(errors[key] + "<br/>");
                    }
                    $("#alert").removeClass('alert-success');
                    $("#alert").addClass('alert-danger');
                }
            }
        });

    });
</script>
@endpush
