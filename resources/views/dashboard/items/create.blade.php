@extends('layouts.dashboard')

@section('content')
    <div class="c-title row no-gutters">
        <div class="col-sm-6">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb c-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('master.items.index') }}">Item data</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add Item</li>
                </ol>
            </nav>
            <h1 class="mb-0">Add item data</h1>
        </div>
        <div class="col-sm-6 text-right"></div>
    </div>
    <div class="alert" id="alert"></div>
    <form class="c-form" novalidate="">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="c-form--title">Item Data</h2>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="type">Item type</label>
                            <select class="form-control" id="type" required="">
                                <option value="SERVICE">Service</option>
                                <option value="ITEM">Item</option>
                            </select>
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="item-id">Item ID</label>
                            <input class="form-control" id="item-id" value="" readonly="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="c-form--label" for="description">Description</label>
                    <input class="form-control" id="description" value="" required="">
                    <div class="invalid-feedback">Data invalid.</div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="item-group">Item group</label>
                            <select class="form-control" id="item-group" required="">
                                <option value="BUBBLE_WASH">Bubble wash</option>
                                <option value="SCRUB_WASH">Scrub wash</option>
                                <option value="SQUEEKLY_CLEAN_WASH">Squeeky clean wash</option>
                            </select>
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="sub-category">Sub category</label>
                            <select class="form-control" id="sub-category" required="">
                                <option value="SINGLE_SEAT_STROLLER">Single seat stroller</option>
                                <option value="DOUBLE_SEAT_STROLLER">Double seat stroller</option>
                            </select>
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="c-form--label" for="price-id">Price</label>
                    <div class="row">
                        <div class="col-sm-6">
                            <select class="form-control" id="price-id" required="">
                                @foreach($prices as $p)
                                    <option value="{{$p->id}}">{{$p->name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                        <div class="col-sm-6">
                            <input class="form-control" id="price-value" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <h2 class="c-form--title">Item Detail (Type service only)</h2>
                <div class="form-group">
                    <label class="c-form--label" for="product">Product</label>
                    <input class="form-control" id="product" required="">
                    <div class="invalid-feedback">Data invalid.</div>
                </div>
                <div class="form-group">
                    <label class="c-form--label" for="service">Service</label>
                    <input class="form-control" id="service" value="" required="">
                    <div class="invalid-feedback">Data invalid.</div>
                </div>
            </div>
        </div>
        <hr class="my-4">
        <div class="text-right">
            <a href="{{ route('master.items.index')}}" class="btn btn-light mr-2" type="button">Cancel</a>
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </form>
@stop
@push('scripts')
<script>
    function firstLoad() {
        var token = getCookie('token');
        $.ajax({
            type: 'GET',
            url: '{{url('api/bebewash/items/create')}}',
            data: 'token=' + token ,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (msg) {
                if (msg.success) {
                    $("#item-id").val(msg.new_item_id);
                }
            },
            error: function(msg) {
                console.log(msg.responseJSON.message)
            }
        });
    }

    $(document).ready(function() {
        firstLoad();
    });
    $("form").submit(function() {
        $("button[type=submit]").hide();
        $.ajax({
            type: 'POST',
            url: '{{url('api/bebewash/items')}}',
            data: '{ "token":"' + getCookie("token") + '",' +
            '"price_id":"' + $("#price-id").val() + '",' +
            '"type":"' + $("#type").val() + '",' +
            '"group":"' + $("#item-group").val() + '",' +
            '"subcategory":"' + $("#sub-category").val() + '",' +
            '"description":"' + $("#description").val() + '",' +
            '"name_of_product":"' + $("#product").val() + '",' +
            '"name_of_service":"' + $("#service").val() + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (msg) {
                if (msg.success) {
                    $("#alert").addClass('alert-success');
                    $("#alert").removeClass('alert-danger');
                    $("#alert").html("Item inserted");
                    $("button[type=submit]").show();
                    $('form').trigger("reset");
                    $("#item-id").val(msg.next_code);
                }
            },
            error: function(msg) {
                $("button[type=submit]").show();
                var errors = $.parseJSON(msg.responseText);

                if (errors.errors) {
                    errors = errors.errors;
                    $("#alert").html("");
                    for (var key in errors) {
                        $("#alert").append(errors[key] + "<br/>");
                    }
                    $("#alert").removeClass('alert-success');
                    $("#alert").addClass('alert-danger');
                }
            }
        });
       return false;
    });

    $("#type").change(function() {
       if($(this).val() == 'ITEM')  {
           $("#product").attr('readonly','');
           $("#service").attr('readonly','');
       }
        else {
           $("#product").removeAttr('readonly');
           $("#service").removeAttr('readonly');
       }
    });
</script>
@endpush