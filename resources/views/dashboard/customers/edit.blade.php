@extends('layouts.dashboard')

@section('content')
    <div class="c-title row no-gutters">
        <div class="col-sm-6">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb c-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('master.items.index') }}">Customer data</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Customer</li>
                </ol>
            </nav>
            <h1 class="mb-0">Edit customer data</h1>
        </div>
        <div class="col-sm-6 text-right"></div>
    </div>
    <div class="alert" id="alert"></div>
    <form class="c-form" novalidate="">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="c-form--title">Personal Data</h2>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="business-partner">Business Partner</label>
                            <select class="form-control" id="business-partner" required="">
                                <option value="CUSTOMER">Customer</option>
                                <option value="VENDOR">Vendor</option>
                                <option value="ENDORSER">Endorser</option>
                            </select>
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="business-partner-id">Business Partner ID</label>
                            <input class="form-control" id="business-partner-id" value="" readonly="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="c-form--label" for="name">Name</label>
                    <input class="form-control" id="name" value="" required="">
                    <div class="invalid-feedback">Data invalid.</div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="birthday">Birthday</label>
                            <input class="form-control datetimepicker" id="birthday" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="gender">Gender</label>
                            <div class="mt-1" id="gender">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input form-check-radio" id="male" type="radio" name="gender" value="male" required="">
                                    <label class="form-check-label" for="male">Male</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input form-check-radio" id="female" type="radio" name="gender" value="female" required="">
                                    <label class="form-check-label" for="female">Female</label>
                                </div>
                            </div>
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="c-form--label" for="religion">Religion</label>
                    <select class="form-control" id="religion" required="">
                        <option value="kristen">Kristen</option>
                        <option value="katolik">Katolik</option>
                        <option value="islam">Islam</option>
                        <option value="hindu">Hindu</option>
                        <option value="buddha">Buddha</option>
                    </select>
                    <div class="invalid-feedback">Data invalid.</div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="telephone">Telephone</label>
                            <input class="form-control" id="telephone" value="081122228811" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="email">Email</label>
                            <input class="form-control" id="email" value="" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <h2 class="c-form--title">Bebe’s Data</h2>
                <div class="form-group">
                    <label class="c-form--label" for="bebes-name">Bebe's name</label>
                    <input class="form-control" id="bebes-name" required="">
                    <div class="invalid-feedback">Data invalid.</div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="bebe-birthday">Bebe's birthday</label>
                            <input class="form-control datetimepicker" id="bebe-birthday" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="bebes-gender">Bebe's gender</label>
                            <div class="mt-1" id="bebes-gender">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input form-check-radio" id="bebes-male" type="radio" name="bebes-gender" value="male" required="">
                                    <label class="form-check-label" for="bebes-male">Male</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input form-check-radio" id="bebes-female" type="radio" name="bebes-gender" value="female" required="">
                                    <label class="form-check-label" for="bebes-female">Female</label>
                                </div>
                            </div>
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-sm-6">
                <h2 class="c-form--title">Address</h2>
                <div class="form-group">
                    <label class="c-form--label" for="address">Address</label>
                    <textarea class="form-control" id="address"></textarea>
                    <div class="invalid-feedback">Data invalid.</div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="country">Country</label>
                            <input class="form-control" id="country" value="Indonesia" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="city">City</label>
                            <input class="form-control" id="city" value="" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="kecamatan">Kecamatan</label>
                            <input class="form-control" id="kecamatan" value="" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="kelurahan">Kelurahan</label>
                            <input class="form-control" id="kelurahan" value="" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <h2 class="c-form--title">Shipping Address</h2>
                <div class="form-group">
                    <div class="mt-1" id="gender">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-check-box" id="shipping" type="checkbox" name="shipping" value="1">
                            <label class="form-check-label" for="shipping">Address and shipping information are the same</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="c-form--label" for="shipping-address">Address</label>
                    <textarea class="form-control" id="shipping-address"></textarea>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="shipping-country">Country</label>
                            <input class="form-control" id="shipping-country" value="Indonesia" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="shipping-city">City</label>
                            <input class="form-control" id="shipping-city" value="" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="shipping-kecamatan">Kecamatan</label>
                            <input class="form-control" id="shipping-kecamatan" value="" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="shipping-kelurahan">Kelurahan</label>
                            <input class="form-control" id="shipping-kelurahan" value="" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="my-4">
        <div class="text-right">
            <a href="{{ route('master.customers.index')}}" class="btn btn-light mr-2" type="button">Cancel</a>
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </form>
@stop
@push('scripts')
<script>
    function firstLoad() {
        var token = getCookie('token');
        $.ajax({
            type: 'GET',
            url: '{{url('api/bebewash/customers/'.$id.'/edit')}}',
            data: 'token=' + token ,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (msg) {
                if (msg.success) {
                    $("#business-partner-id").val(msg.customer.customer_id);
                    $("#name").val(msg.customer.name);
                    $("#birthday").val(msg.customer.birthday);
                    $("#gender").val(msg.customer.gender);
                    $("#religion").val(msg.customer.religion);
                    $("#telephone").val(msg.customer.phone_number);
                    $("#email").val(msg.customer.email);
                    $("#bebes-name").val(msg.customer.bebe_name);
                    $("#bebe-birthday").val(msg.customer.bebe_birthday);
                    $("#address").val(msg.customer.address);
                    $("#city").val(msg.customer.city);
                    $("#country").val(msg.customer.country);
                    $("#kecamatan").val(msg.customer.kecamatan);
                    $("#kelurahan").val(msg.customer.kelurahan);
                    $("#shipping-address").val(msg.customer.shipping_address);
                    $("#shipping-city").val(msg.customer.shipping_city);
                    $("#shipping-country").val(msg.customer.shipping_country);
                    $("#shipping-kecamatan").val(msg.customer.shipping_kecamatan);
                    $("#shipping-kelurahan").val(msg.customer.shipping_kelurahan);
                    $("#bebes-gender").val(msg.customer.bebe_gender);
                    if(msg.customer.check_same_address == 1) {
                        $("#shipping").prop('checked', true);
                    }
                    $("#shipping").change();
                }
            },
            error: function(msg) {
                console.log(msg.responseJSON.message)
            }
        });
    }

    $(document).ready(function() {
        firstLoad();
    });

    $("form").submit(function() {
        var same_address = $("#shipping").is(":checked") ? 1 : 0;
        $("button[type=submit]").hide();
        $.ajax({
            type: 'PATCH',
            url: '{{url('api/bebewash/customers/'.$id)}}',
            data: '{ "token":"' + getCookie("token") + '",' +
            '"partner_type":"' + $("#business-partner").val() + '",' +
            '"name":"' + $("#name").val() + '",' +
            '"birthday":"' + $("#birthday").val() + '",' +
            '"gender":"' + $("#gender").val() + '",' +
            '"religion":"' + $("#religion").val() + '",' +
            '"phone_number":"' + $("#telephone").val() + '",' +
            '"email":"' + $("#email").val() + '",' +
            '"bebe_name":"' + $("#bebes-name").val() + '",' +
            '"bebe_birthday":"' + $("#bebe-birthday").val() + '",' +
            '"address":"' + $("#address").val() + '",' +
            '"city":"' + $("#city").val() + '",' +
            '"country":"' + $("#country").val() + '",' +
            '"kecamatan":"' + $("#kecamatan").val() + '",' +
            '"kelurahan":"' + $("#kelurahan").val() + '",' +
            '"shipping_address":"' + $("#shipping-address").val() + '",' +
            '"shipping_city":"' + $("#shipping-city").val() + '",' +
            '"shipping_country":"' + $("#shipping-country").val() + '",' +
            '"shipping_kecamatan":"' + $("#shipping-kecamatan").val() + '",' +
            '"shipping_kelurahan":"' + $("#shipping-kelurahan").val() + '",' +
            '"check_same_address":"' + same_address + '",' +
            '"bebe_gender":"' + $("#bebes-gender").val() + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (msg) {
                if (msg.success) {
                    $("#alert").addClass('alert-success');
                    $("#alert").removeClass('alert-danger');
                    $("#alert").html("Customer updated");
                    $("button[type=submit]").show();
                    firstLoad();
                }
            },
            error: function(msg) {
                $("button[type=submit]").show();
                var errors = $.parseJSON(msg.responseText);

                if (errors.errors) {
                    errors = errors.errors;
                    $("#alert").html("");
                    for (var key in errors) {
                        $("#alert").append(errors[key] + "<br/>");
                    }
                    $("#alert").removeClass('alert-success');
                    $("#alert").addClass('alert-danger');
                }
            }
        });
        return false;
    });

    $("#shipping").change(function() {
        if($(this).prop('checked')) {
            $("#shipping-address").attr('readonly', '');
            $("#shipping-country").attr('readonly', '');
            $("#shipping-city").attr('readonly', '');
            $("#shipping-kelurahan").attr('readonly', '');
            $("#shipping-kecamatan").attr('readonly', '');
        }
        else {
            $("#shipping-address").removeAttr('readonly');
            $("#shipping-country").removeAttr('readonly');
            $("#shipping-city").removeAttr('readonly');
            $("#shipping-kecamatan").removeAttr('readonly');
            $("#shipping-kelurahan").removeAttr('readonly');
        }
    });

</script>
@endpush