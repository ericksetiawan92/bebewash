@extends('layouts.dashboard')

@section('content')
    <div class="c-title row no-gutters">
        <div class="col-sm-6">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb c-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('master.banks.index') }}">Bank data</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add bank</li>
                </ol>
            </nav>
            <h1 class="mb-0">Add bank data</h1>
        </div>
        <div class="col-sm-6 text-right"></div>
    </div>
    <div class="alert" id="alert"></div>
    <form class="c-form" novalidate="">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="c-form--title">Bank Data</h2>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="bank-id">Bank ID</label>
                            <input class="form-control" id="bank-id" value="" readonly="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="bank-name">Bank name</label>
                            <select class="form-control" id="bank-name" required="">
                                @foreach($banks as $b)
                                    <option value="{{ $b->id }}">{{ $b->name }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback" id="bank-invalid">Data invalid.</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="account-name">Account name</label>
                            <input class="form-control" id="account-name" required="">
                            <div class="invalid-feedback">Data invalid.</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="c-form--label" for="account-number">Account number</label>
                            <input class="form-control" id="account-number" required="">
                            <div class="invalid-feedback" id="account-number-invalid">Data invalid.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="my-4">
        <div class="text-right">
            <a href="{{ route('master.banks.index')}}" class="btn btn-light mr-2" type="button">Cancel</a>
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </form>
@stop
@push('scripts')
<script>
    function firstLoad(token) {
        var token = getCookie('token');
        $.ajax({
            type: 'GET',
            url: '{{url('api/bebewash/banks/create')}}',
            data: 'token=' + token ,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (msg) {
                if (msg.success) {
                    $("#bank-id").val(msg.new_bank_account_id)
                }
            },
            error: function(msg) {
                console.log(msg.responseJSON.message)
            }
        });
    }

    $(document).ready(function() {
        firstLoad();
    });

    $("form").submit(function() {
        $("button[type=submit]").hide();
        $.ajax({
            type: 'POST',
            url: '{{url('api/bebewash/banks')}}',
            data: '{ "token":"' + getCookie("token") + '",' +
            '"new_bank_account_id":"' + $("#bank-id").val() + '",' +
            '"bank_id":"' + $("#bank-name").val() + '",' +
            '"account_name":"' + $("#account-name").val() + '",' +
            '"account_number":"' + $("#account-number").val() + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (msg) {
                if (msg.success) {
                    $("#alert").addClass('alert-success');
                    $("#alert").removeClass('alert-danger');
                    $("#alert").html("Bank inserted");
                    $("button[type=submit]").show();
                    $('form').trigger("reset");
                    firstLoad();
                }
            },
            error: function(msg) {
                $("button[type=submit]").show();
                var errors = $.parseJSON(msg.responseText);

                if (errors.errors) {
                    errors = errors.errors;
                    $("#alert").html("");
                    for (var key in errors) {
                        $("#alert").append(errors[key] + "<br/>");
                    }
                    $("#alert").removeClass('alert-success');
                    $("#alert").addClass('alert-danger');
                    firstLoad();
                }
            }
        });
       return false;
    });
</script>
@endpush