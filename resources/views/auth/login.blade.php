<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <meta name="description" content="Bebewash Design, Component and Prototype">
        <meta name="author" content="Bebewash">
        <meta name="keywords" content="HTML, PUG, SCSS, CSS, XHTML, JavaScript">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/images/ic-logo-bebewash.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/images/ic-logo-bebewash.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/images/ic-logo-bebewash.png') }}">
        <link rel="mask-icon" href="{{ asset('public/images/ic-logo-bebewash.png') }}" color="#ffffff">
        <meta name="theme-color" content="#225F90">
        <title>Bebewash | Login</title>
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/images/ic-logo-bebewash.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/images/ic-logo-bebewash.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/images/ic-logo-bebewash.png') }}">
        <link rel="mask-icon" href="{{ asset('public/images/ic-logo-bebewash.png') }}" color="#ffffff">
        <link rel="stylesheet" href="{{ url('public/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ url('public/css/font-awesome.css') }}">
        <link rel="stylesheet" href="{{ url('public/css/fonts.min.css') }}">
        <link rel="stylesheet" href="{{ url('public/css/main.min.css') }}">
    </head>

    <body>
        {{--<script id="__bs_script__">//<![CDATA[--}}
        {{--document.write("<script async src='/browser-sync/browser-sync-client.js?v=2.26.7'><\/script>".replace("HOST", location.hostname));--}}
        {{--//]]></script><script async="" src="/browser-sync/browser-sync-client.js?v=2.26.7"></script>--}}

        <div class="p-access">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-sm-1">
                        <div class="row align-items-center vh-100">
                            <div class="col-sm-6 offset-sm-3">
                                <div class="text-center mb-4"><img src="{{ url('public/images/logo-bebewash.png') }}" height="77"></div>
                                <form class="c-form needs-validation" novalidate="">
                                    <h2 class="c-form--title">Welcome</h2>
                                    <h2 class="c-form--title" id="user-not-found" style="color: #f55d42"></h2>
                                    <div class="form-group">
                                        <label class="c-form--label" for="validationCustom01">Email</label>
                                        <input class="form-control" id="validationCustom01" type="email" required="">
                                        <div class="invalid-feedback" id="invalid-email"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="c-form--label" for="validationCustom02">Password</label>
                                        <input class="form-control" id="validationCustom02" type="password" required="">
                                        <div class="invalid-feedback" id="invalid-password"></div>
                                    </div>
                                    <hr class="my-4">
                                    <div class="text-right">
                                        <button class="btn btn-primary btn-block" type="submit">Login</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="{{ asset('public/js/jquery-1.11.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/js/moment.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/js/moment-with-locales.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/js/bootstrap-datetimepicker.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/js/main.js') }}"></script>
        <script>
            function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                var expires = "expires="+ d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            }

            function getCookie(cname) {
                var name = cname + "=";
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(';');
                for(var i = 0; i <ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }

            function isAuthenticated() {
                var token = getCookie('token');
                $.ajax({
                    type: 'GET',
                    url: '{{url('api/bebewash/authenticated')}}',
                    data: 'token=' + token ,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (msg) {
                        window.location = "{{ url('dashboard') }}";
                    },
                    error: function(msg) {
                        console.log(msg.responseJSON.message)
                    }
                });
            }

            $(document).ready(function() {
                isAuthenticated();
            });

        </script>
        <script>
            $("form").on('submit', function() {
                $('#invalid-email').html();
                $('#invalid-password').html();
                $('#user-not-found').html();
                $.ajax({
                    type: 'POST',
                    url: '{{url('api/bebewash/login')}}',
                    data: '{ "email":"' + $("#validationCustom01").val() + '",' +
                    '"password":"' + $("#validationCustom02").val() + '"}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (msg) {
                        if (msg.success) {
                            setCookie('token', msg.token, 180);
                            window.location = "{{ url('dashboard') }}";
                        }
                    },
                    error: function(msg) {
                        var errors = $.parseJSON(msg.responseText);
                        if (errors.errors) {
                            errors = errors.errors;
                            if (errors.email) $('#invalid-email').html(errors.email)
                            if (errors.password) $('#invalid-password').html(errors.password)
                        } else if (!msg.responseJSON.success) {
                            $('#user-not-found').html(msg.responseJSON.message);
                        }
                    }
                });
                return false;
            });

        </script>
    </body>
</html>
