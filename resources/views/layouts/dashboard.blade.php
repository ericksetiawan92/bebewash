<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <meta name="description" content="Bebewash Design, Component and Prototype">
        <meta name="author" content="Bebewash">
        <meta name="keywords" content="HTML, PUG, SCSS, CSS, XHTML, JavaScript">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/images/ic-logo-bebewash.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/images/ic-logo-bebewash.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/images/ic-logo-bebewash.png') }}">
        <link rel="mask-icon" href="{{ asset('public/images/ic-logo-bebewash.png') }}" color="#ffffff">
        <meta name="theme-color" content="#225F90">
        <title>Bebewash</title>
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/images/ic-logo-bebewash.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/images/ic-logo-bebewash.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/images/ic-logo-bebewash.png') }}">
        <link rel="mask-icon" href="{{ asset('public/images/ic-logo-bebewash.png') }}" color="#ffffff">
        <link rel="stylesheet" href="{{ url('public/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ url('public/css/font-awesome.css') }}">
        <link rel="stylesheet" href="{{ url('public/css/fonts.min.css') }}">
        <link rel="stylesheet" href="{{ url('public/css/main.min.css') }}">
        @stack('styles')
    </head>

    <body>
        <header class="c-header row no-gutters">
            <div class="c-bars"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="svg replaced-svg"><path d="M3 12h18M3 6h18M3 18h18"></path></svg></div>
            <div class="c-search"></div>
            <div class="c-user dropdown">
                <div class="c-user--inner row no-gutters" id="dropdown-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="c-user--avatar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="svg replaced-svg"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg></div>
                    <div class="c-user--name" id="user-name"></div>
                    <div class="c-user--arrow"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="svg replaced-svg"><path d="M6 9l6 6 6-6"></path></svg></div>
                </div>
                <div class="dropdown-menu" aria-labelledby="dropdown-user"><a class="dropdown-item" href="#">Profile</a><a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></div>
            </div>
        </header>
        <div class="c-sidebar">
            <div class="c-sidebar--logo mb-5">
                <div class="mb-4 text-center"><img class="logo-full" src="{{ asset('public/images/logo-bebewash.png') }}" height="64"><img class="logo-icon" src="{{ asset('public/images/ic-logo-bebewash.png') }}" height="40"></div><a class="btn btn-primary btn-block btn-lg" href="#"><span> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="svg replaced-svg"><path d="M12 5v14m-7-7h14"></path></svg></span><span>Add Sales Order</span></a>
            </div>
            <nav class="c-nav">
                <div class="c-nav--item" id="sales">
                    <a class="" href="./../sales/list-sales">
                        <span class="mr-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="svg replaced-svg"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4zM3 6h18m-5 4a4 4 0 0 1-8 0"></path></svg></span><span>Sales Order</span>
                    </a>
                </div>
                <div class="c-nav--item" id="invoice">
                    <a href="./../sales/list-invoice">
                        <span class="mr-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="svg replaced-svg"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><path d="M14 2v6h6m-4 5H8m8 4H8m2-8H8"></path></svg></span><span>Invoices</span>
                    </a></div>
                <div class="c-nav--item" id="customer">
                    <a href="{{ route('master.customers.index') }}">
                        <span class="mr-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="svg replaced-svg"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87m-4-12a4 4 0 0 1 0 7.75"></path></svg></span><span>Customer</span>
                    </a>
                </div>
                <div class="c-nav--item" id="item">
                    <a href="{{ route('master.items.index') }}" @if(isset($menu) && $menu == 'item')class="is-active"@endif>
                        <span class="mr-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="svg replaced-svg"><path d="M16.5 9.4l-9-5.19M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><path d="M3.27 6.96L12 12.01l8.73-5.05M12 22.08V12"></path></svg></span><span>Item</span>
                    </a>
                </div>
                <div class="c-nav--item" id="price">
                    <a href="./../master/list-price">
                        <span class="mr-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="svg replaced-svg"><path d="M12 1v22m5-18H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg></span><span>Price</span>
                    </a>
                </div>
                <div class="c-nav--item" id="vehicle">
                    <a href="{{ route('master.vehicles.index') }}" @if(isset($menu) && $menu == 'vehicle')class="is-active"@endif>
                        <span class="mr-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="svg replaced-svg"><path d="M1 3h15v13H1zm15 5h4l3 3v5h-7V8z"></path><circle cx="5.5" cy="18.5" r="2.5"></circle><circle cx="18.5" cy="18.5" r="2.5"></circle></svg></span><span>Vehicle</span>
                    </a>
                </div>
                <div class="c-nav--item" id="bank">
                    <a href="{{ route('master.banks.index') }}" @if(isset($menu) && $menu == 'bank')class="is-active"@endif>
                        <span class="mr-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="svg replaced-svg"><rect x="1" y="4" width="22" height="16" rx="2" ry="2"></rect><path d="M1 10h22"></path></svg></span><span>Bank</span>
                    </a>
                </div>
                <div class="c-nav--item" id="agent">
                    <a href="{{ route('master.agents.index') }}" @if(isset($menu) && $menu == 'agent')class="is-active"@endif>
                        <span class="mr-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="svg replaced-svg"><rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect><path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path></svg></span><span>Agent</span>
                    </a>
                </div>
                <div class="c-nav--item" id="user">
                    <a href="#">
                        <span class="mr-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="svg replaced-svg"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg></span><span>User</span>
                    </a>
                </div>
            </nav>
        </div>
        <main class="main">

            @yield('content')
        </main>

        {{--<script id="__bs_script__">//<![CDATA[--}}
        {{--document.write("<script async src='/browser-sync/browser-sync-client.js?v=2.26.7'><\/script>".replace("HOST", location.hostname));--}}
        {{--//]]></script><script async="" src="/browser-sync/browser-sync-client.js?v=2.26.7"></script>--}}


        <form action="{{ url('api/bebewash/logout') }}" method="POST" id="logout-form" style="display: none;">
            <input type="hidden" name="token">
        </form>
        <script type="text/javascript" src="{{ asset('public/js/jquery-1.11.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/js/moment.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/js/moment-with-locales.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/js/bootstrap-datetimepicker.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/js/main.js') }}"></script>
        <script>
            function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                var expires = "expires="+ d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            }
            function getCookie(cname) {
                var name = cname + "=";
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(';');
                for(var i = 0; i <ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }
            function deleteCookie( name ) {
                document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            }
            function isAuthenticated() {
                var token = getCookie('token');
                $.ajax({
                    type: 'GET',
                    url: '{{url('api/bebewash/authenticated')}}',
                    data: 'token=' + token ,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (msg) {
                        console.log(msg.message);
                        $('#user-name').html(msg.user.name)
                    },
                    error: function(msg) {
                        if(!msg.responseJSON.success) {
                            window.location = "{{ url('login') }}";
                        }
                    }
                });
            }
            $(document).ready(function() {
                var url      = window.location.href;
                url = url.split("/");
                var page = url[url.length - 1];
                if(page != 'login')
                    isAuthenticated();

                $("#logout-form input[name=token]").val(getCookie('token'));
            });

            function logout() {
                var token = getCookie('token');
                deleteCookie('token');
                window.location = "{{ url('logout') }}?token="+token;
            }

        </script>
        @stack('scripts')
    </body>
</html>